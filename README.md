# HTML5Video #
![Release](https://img.shields.io/badge/release-v1.1.1-green.svg)

The aim of this project is to provide a easy way to use HTML5 video player.

## Features

* Easy to use
* Super lightweight
* 16x9 auto size
* Custom poster + button play
* Control all players in the same time
* Methods (Global and local)
* Better way integration (JS or Attr)
* Easy Events Callback
* Debug Mode
* Custom Name
* Mods (Absolute, FancyBox, FIXED)
* Add Global String events on Player by Attr
* All events return the HTML5Video
* Compatible accessibility

## TO DO

# Dependencies

* jQuery
* FancyBox 3.2.5 (*optional - only for FancyBox mod*)
* Youtube `<script src="https://www.youtube.com/iframe_api"></script>` (*optional - only for Youtube api*)

# Installing

* `bower install https://bitbucket.org/version10/v10core-html5video.git --save`

## Using

```javascript
    var v = new V10Core.HTML5Video({OPTIONS});
```

## Options.core

Core | Type | Default | Description
------------ | ------ | --------- | -------------
container | String | `null` | container name selector
name | String | `''` | Custom name (optional)
api | String | `'html5'` | API (html5 / youtube / vimeo / embed)
debugMode | Boolean | `false` | Active Debug Mode (Logs Help)

## Options.media

Media | Type | Default | Description
------------ | ------ | --------- | -------------
source | String | `'http://www.w3schools.com/html/mov_bbb.mp4'` | video source

## Options.ui

Media | Type | Default | Description
------------ | ------ | --------- | -------------
poster | String | `''` | poster image
btnPlay | String | `'<i class='fa fa-play-circle-o fa-5x' aria-hidden='true'></i>'` | btn play

## Options.vParams

Video Params | Type | Default | Description
------------ | ------ | --------- | -------------
autoplay | Boolean | `false` | Auto play the player on init
controls | Boolean | `true` | Show the control
width | String | `'100%'` | width of the player
height | String | `'100%'` | height of the player

## Options.events

Events | Type | Default | Description
------------ | ------ | --------- | ----------------
PLAYER_UI_READY | Function | `null` | Call After the UI ready (poster + buttons)
START | Function | `null` | 
END | Function | `null` | 
PLAY | Function | `null` | 
PAUSE | Function | `null` | 
SEEKED | Function | `null` | 
CURRENT_TIME_CHANGE | Function | `null` | 
VOLUME_CHANGE | Function | `null` | 
MUTE | Function | `null` | 
UNMUTE | Function | `null` | 
ENTER_FULL_SCREEN | Function | `null` | 
EXIT_FULL_SCREEN | Function | `null` | 
ERROR | Function | `null` | 
META_LOADED | Function | `null` | 

## Options.mod

Mod | Description
------------ | ---------
DEFAULT | NO Mod (Default player)
ABSOLUTE | Show the player on top while playing
FANCY_BOX | Show the player on FancyBox3
FIXED | Show the player on and fallow

## Example HTML5 Video Player (Default + PLAY event)

```javascript
    jQuery(document).ready(function($) {

        var v = new V10Core.HTML5Video({
            core:{
                container:"[data-player-container]"
            },
            media:{
                 source:"http://www.w3schools.com/html/mov_bbb.mp4"
            },
            events: {
                PLAY: function () {
                    console.log("play!");
                }
            }
        });

    });
```

## Example HTML5 Video Player (Custom UI)

```javascript
   jQuery(document).ready(function($) {

       var v = new V10Core.HTML5Video({
           core:{
               container:"[data-player-container]"
           },
           media:{
                source:"http://www.w3schools.com/html/mov_bbb.mp4"
           },
           ui: {
               poster: "http://via.placeholder.com/990x556"
           }
       });

   });
```

## Example HTML5 Video Player (Custom UI + Fancybox)

```javascript
    jQuery(document).ready(function($) {

        var v = new V10Core.HTML5Video({
            core:{
                container:"[data-player-container]"
            },
            media:{
                 source:"http://www.w3schools.com/html/mov_bbb.mp4"
            },
            vParams:{
                autoplay:true
            },
            ui: {
                poster: "http://via.placeholder.com/990x556"
            },
            mod:V10Core.HTML5Video.mods.FANCY_BOX
        });

    });
```

## Example HTML5 Video Player (Open Fancybox on page load)

```javascript
    jQuery(document).ready(function($) {

        var v = new V10Core.HTML5Video({
            media:{
                 source:"http://www.w3schools.com/html/mov_bbb.mp4"
            },
            vParams:{
                autoplay:true
            },
            mod:V10Core.HTML5Video.mods.FANCY_BOX
        });

    });
```

## Example HTML5 Video Player (Absolute)

```javascript
       jQuery(document).ready(function($) {

           var v = new V10Core.HTML5Video({
               core:{
                   container:"[data-player-container]"
               },
               media:{
                    source:"http://www.w3schools.com/html/mov_bbb.mp4"
               },
               vParams:{
                   autoplay:true
               },
               ui: {
                   poster: "http://via.placeholder.com/990x556"
               },
               mod:V10Core.HTML5Video.mods.ABSOLUTE
           });

       });
```

## Example HTML5 Video Player (Fixed)

```javascript
    jQuery(document).ready(function($) {

        var v = new V10Core.HTML5Video({
            core:{
                container:"[data-player-container]"
            },
            media:{
                 source:"http://www.w3schools.com/html/mov_bbb.mp4"
            },
            vParams:{
                autoplay:true
            },
            ui: {
                poster: "http://via.placeholder.com/990x556"
            },
            mod:V10Core.HTML5Video.mods.FIXED
        });

    });
```

```CSS
    .v10core-html5video-mod-fixed{
        max-width: 25vw;
        max-height: 25vh;
        top:15px;
        left:15px;
    }
```

## Example HTML5 Video Player (HTML Attr method)

```HTML
    <div data-v10core-html5video='{"media": {"source":"http://www.w3schools.com/html/mov_bbb.mp4"}}'></div>
```

## Example HTML5 Video Player (HTML Custom Attr method + Ajax)

```HTML
    <div data-custom-attr='{"media": {"source":"http://www.w3schools.com/html/mov_bbb.mp4"}}'></div>
```

```javascript
    jQuery(document).ready(function($) {
        V10Core.HTML5Video.initPlayerByAttr("data-custom-attr");
    });
```

## Example HTML5 Video Player (HTML Attr method + Ajax)

```HTML
    <div data-v10core-html5video='{"media": {"source":"http://www.w3schools.com/html/mov_bbb.mp4"}}'></div>
```

```javascript
    jQuery(document).ready(function($) {
        V10Core.HTML5Video.initPlayerByAttr(V10Core.HTML5Video.vPlayerAttribute);
    });
```


## Global Methods

*Pause*

Pause all videos

```
    V10Core.HTML5Video.pause();
```

*Stop*

Stop all videos

```
    V10Core.HTML5Video.stop();
```

*players*

Return the list of HTML5Video

```
    V10Core.HTML5Video.players
```

*count*

Return the number of HTML5Video

```
    V10Core.HTML5Video.count
```

*initPlayerByAttr*

Create new players by the attribute

```
    V10Core.HTML5Video.initPlayerByAttr(V10Core.HTML5Video.vPlayerAttribute);

    V10Core.HTML5Video.initPlayerByAttr("data-custom-attr");
```



## Local Methods

*Play*

Play the video

```
    v.play();
```

*Pause*

Pause the video

```
    v.pause();
```

*Stop*

Stop the video

```
    v.stop();
```

*Change*

Change the video source (source, CUSTOM_HTML5_Video_PARAMS)

```
    rc.change("http://www.w3schools.com/html/mov_bbb.mp4", {autoPlay:true});
```

### Credits ###

- Michaël Chartrand (SirKnightDragoon)