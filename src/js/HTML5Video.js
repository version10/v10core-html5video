//HTML5Video v1.2.0
if(typeof V10Core === "undefined") var V10Core = {};

V10Core.HTML5Video = function(options){
    var _this = this;

    //Init Options
    this.options = {};

    //HTML5Video Core options
    this.options.core = {
        name:"",
        container:null,
        api:"html5",
        debugMode:false
    };

    //Media options
    this.options.media = {
        source:"http://www.w3schools.com/html/mov_bbb.mp4"
    }

    this.options.youtube = {
        videoId: 'M7lc1UVf-VE',
        playerVars: {autoplay: 0},
        width: "100%",
        height: "100%"
    }

    //Custom UI option
    this.options.ui = {
        poster:"",
        btnPlay:"<i class='fa fa-play-circle-o fa-5x' aria-hidden='true'></i>"
    }

    //Radio-canada html5 video options
    this.options.vParams = {
        autoplay: false,
        controls: true,
        width: "100%",
        height: "100%"
    };

    //Events options callback
    this.options.events = {
        PLAYER_UI_READY:null,
        PLAYER_READY:null,
        END:null,
        START:null,
        PLAY:null,
        PAUSE:null,
        SEEKED :null,
        CURRENT_TIME_CHANGE:null,
        VOLUME_CHANGE:null,
        MUTE:null,
        UNMUTE:null,
        ENTER_FULL_SCREEN:null,
        EXIT_FULL_SCREEN:null,
        ERROR:null,
        META_LOADED:null
    }

    //Mod options
    this.options.mod = V10Core.HTML5Video.mods.DEFAULT;

    //Add all user options in the options group
    for (key in options.core) {
        this.options.core[key] = options.core[key];
    }

    for (key in options.media) {
        this.options.media[key] = options.media[key];
    }

    for (key in options.youtube) {
        this.options.youtube[key] = options.youtube[key];
    }

    for (key in options.ui) {
        this.options.ui[key] = options.ui[key];
    }

    for (key in options.vParams) {
        this.options.vParams[key] = options.vParams[key];
    }

    for (key in options.events) {
        this.options.events[key] = options.events[key];

        if(typeof options.events[key] == "string"){
            this.options.events[key] = window[options.events[key]];
        }
    }

    //Mod options
    if(options.mod) this.options.mod = options.mod;

    //Add the player in the list
    V10Core.HTML5Video.players.push(this);
    V10Core.HTML5Video.count++;

    //Create UID of the player
    this.uid = "v10core-html5video-" + V10Core.HTML5Video.count;

    //Set default name
    if(this.options.core.name == "") this.options.core.name = this.uid;

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: created");

    //Init player events
    this.initUI();

    //Init the player by mod selected
    if(this.options.ui.poster == ""){
        if(this.options.core.api == "youtube"){
            if(V10Core.HTML5Video.YouTubeIframeAPIReady){
                this.initDefaultPlayer();
                this.eventReady();
            }else{
                this.intervalYouTubeIframeAPIReady = setInterval(function(){
                    if(V10Core.HTML5Video.YouTubeIframeAPIReady){
                        _this.initDefaultPlayer();
                        _this.eventReady();
                        clearInterval(_this.intervalYouTubeIframeAPIReady);
                    }
                }, 500);
            }
        }

        if(this.options.core.api == "html5"){
            this.initDefaultPlayer();
            this.eventReady();
        }
    }
}

//Function initUI
// -----------------------------------------------------
// Init the UI of the player
V10Core.HTML5Video.prototype.initUI = function(){
    //Insert the uid in the core container
    if(this.options.core.container != null){

        //CSS 16x9 Box
        $(this.options.core.container).css({
            position: "relative",
            display: "block",
            height: 0,
            padding: 0,
            overflow: "hidden",
            paddingBottom: "56.25%"
        });

        //Init core ui group
        this.ui = {};

        //Create the player container ui
        this.ui.player = $("<div class='v10core-html5video-ui-player' "+this.uid+"-player></div>");
        this.ui.player.attr("id", this.uid);

        $(this.ui.player).css({
            position: "absolute",
            display: "block",
            width: "100%",
            height: "100%"
        });

        $(this.options.core.container).append(this.ui.player);

        //Add poster image
        if(this.options.ui.poster != ""){

            this.ui.poster = $("<a href='#' class='v10core-html5video-ui-poster' "+this.uid+"-ui-poster><span style='display: none;'>Écouter la vidéo</span></a>");

            this.ui.poster.css({
                position: "absolute",
                top:0,
                left:0,
                display: "block",
                width: "100%",
                height: "100%",
                backgroundImage: "url('"+this.options.ui.poster+"')",
                backgroundRepeat:"no-repeat",
                backgroundSize:"cover",
                zIndex:2,
                cursor:"pointer"
            });

            $(this.options.core.container).append(this.ui.poster);

            if(this.options.ui.btnPlay != ""){
                this.ui.btnPlay = $(this.options.ui.btnPlay);
                this.ui.btnPlay.attr(this.uid+"-ui-btn-play", "");
                this.ui.btnPlay.addClass("v10core-html5video-ui-btn-play");

                this.ui.btnPlay.css({
                    position:"absolute",
                    top: "50%",
                    left: "50%",
                    webkitTransform: "translate(-50%,-50%)",
                    transform: "translate(-50%,-50%)",
                    zIndex:3,
                    pointerEvents: "none"
                });

                $(this.options.core.container).append(this.ui.btnPlay);
            }

            this.ui.poster.on("click", this.onUIEventPlay.bind(this));
        }
    }

    if(this.options.events.PLAYER_UI_READY != null) this.options.events.PLAYER_UI_READY(this);
}

//Function initDefaultPlayer
// -----------------------------------------------------
// Init the default player
V10Core.HTML5Video.prototype.initDefaultPlayer = function(){
    switch(this.options.core.api){
        case "html5":
            this.player = $('<video />', {
                id: this.uid+"-video",
                src: this.options.media.source,
                type: 'video/mp4',
                controls: this.options.vParams.controls,
                autoplay: this.options.vParams.autoplay
            });
            this.player.css({
                width:this.options.vParams.width,
                height:this.options.vParams.height
            });

            this.player.appendTo($("#"+this.uid));
            break;
        case "youtube":
            this.ui.youtube = $('<div />', {
                id: this.uid+"-youtube"
            });
            this.ui.youtube.appendTo($("#"+this.uid));
            this.player = new YT.Player(this.uid+"-youtube", {
                height: this.options.youtube.width,
                width: this.options.youtube.height,
                playerVars:this.options.youtube.playerVars,
                videoId: this.options.youtube.videoId
            });
            break;
    }

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: initDefaultPlayer");
}

//Function initFancyboxPlayer
// -----------------------------------------------------
// Init the fancybox player
V10Core.HTML5Video.prototype.initFancyboxPlayer = function(){
    var _this = this;

    var fancyboxHTML = $("<div></div>");
    var fancyboxPlayerCnt = $("<div style='width: 100%; background: none; overflow: hidden;'></div>");

    fancyboxPlayerCnt.attr("id", this.uid+'-fancybox');

    fancyboxHTML.append(fancyboxPlayerCnt);

    $.fancybox.open(fancyboxHTML.html(), {
        infobar: true,
        buttons : [
            'close'
        ],
        smallBtn : false,
        touch : false,
        onUpdate : function( instance, current ) {
            var width,
                height,
                ratio = 16 / 9,
                video = current.$content;

            if ( video ) {
                video.hide();
                width  = current.$slide.width();
                height = current.$slide.height() - 100;

                if ( height * ratio > width ) {
                    height = width / ratio;
                } else {
                    width = height * ratio;
                }

                video.css({
                    width  : width,
                    height : height
                })

                video.show();
            }
        },
        afterLoad:function(instance, current){

            var video = current.$content;

            video.css({
                background  : "none"
            });

            switch(_this.options.core.api){
                case "html5":
                    _this.player = $('<video />', {
                        id: _this.uid+"-video",
                        src: _this.options.media.source,
                        type: 'video/mp4',
                        controls: _this.options.vParams.controls,
                        autoplay: _this.options.vParams.autoplay
                    });
                    _this.player.css({
                        width:_this.options.vParams.width,
                        height:_this.options.vParams.height
                    });

                    _this.player.appendTo($("#"+_this.uid+"-fancybox"));
                    break;
                case "youtube":
                    _this.ui.youtube = $('<div />', {
                        id: _this.uid+"-youtube"
                    });
                    _this.ui.youtube.appendTo($("#"+_this.uid+"-fancybox"));
                    _this.player = new YT.Player(_this.uid+"-youtube", {
                        height: _this.options.youtube.width,
                        width: _this.options.youtube.height,
                        playerVars:_this.options.youtube.playerVars,
                        videoId: _this.options.youtube.videoId
                    });
                    break;
            }
        }
    });

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: initFancyboxPlayer");
}

//Function initAbsolutePlayer
// -----------------------------------------------------
// Init the absolute player
V10Core.HTML5Video.prototype.initAbsolutePlayer = function(){
    var _this = this;

    var absolutePlayerCnt = $("<div></div>");
    absolutePlayerCnt.attr("id", this.uid+'-absolute');
    absolutePlayerCnt.addClass('v10core-html5video-mod-absolute');
    absolutePlayerCnt.css({
        width:$(this.options.core.container).innerWidth(),
        height:$(this.options.core.container).innerHeight(),
        position:"absolute",
        zIndex:"9999999999",
        top:$(this.options.core.container).offset().top,
        left:$(this.options.core.container).offset().left,
        background:"#000"
    });
    $("body").append(absolutePlayerCnt);

    $(window).on("resize", function(){
        console.log("resize");
        absolutePlayerCnt.css({
            width:$(_this.options.core.container).innerWidth(),
            height:$(_this.options.core.container).innerHeight(),
            top:$(_this.options.core.container).offset().top,
            left:$(_this.options.core.container).offset().left
        });
    });

    this.player = $('<video />', {
        id: this.uid+"-video",
        src: this.options.media.source,
        type: 'video/mp4',
        controls: this.options.vParams.controls,
        autoplay: this.options.vParams.autoplay
    });
    this.player.css({
        width:this.options.vParams.width,
        height:this.options.vParams.height
    });
    this.player.appendTo($("#"+this.uid+'-absolute'));

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: initAbsolutePlayer");
}

//Function initFixedPlayer
// -----------------------------------------------------
// Init the fixed player
V10Core.HTML5Video.prototype.initFixedPlayer = function(){
    var _this = this;

    var fixedPlayerCnt = $("<div></div>");
    fixedPlayerCnt.attr("id", this.uid+'-fixed');
    fixedPlayerCnt.addClass('v10core-html5video-mod-fixed');
    fixedPlayerCnt.css({
        position:"fixed",
        zIndex:"9999999999"
    });
    $("body").append(fixedPlayerCnt);

    function updateFixedSize(){
        var ratio = 16 / 9;

        var width  = parseInt(fixedPlayerCnt.css("maxWidth"));
        var height = parseInt(fixedPlayerCnt.css("maxHeight"));

        if ( height * ratio > width ) {
            height = width / ratio;
        } else {
            width = height * ratio;
        }

        fixedPlayerCnt.css({
            width  : width,
            height : height
        })
    }

    $(window).on("resize", updateFixedSize);

    updateFixedSize();

    this.player = $('<video />', {
        id: this.uid+"-video",
        src: this.options.media.source,
        type: 'video/mp4',
        controls: this.options.vParams.controls,
        autoplay: this.options.vParams.autoplay
    });
    this.player.css({
        width:this.options.vParams.width,
        height:this.options.vParams.height
    });
    this.player.appendTo($("#"+this.uid+'-fixed'));

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: initAbsolutePlayer");
}

//Method play
// -----------------------------------------------------
// play the player
V10Core.HTML5Video.prototype.play = function(){
    switch(this.options.core.api){
        case "html5":
            this.player[0].play();
            break;
        case "youtube":
            this.player.playVideo();
            break;
    }

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: play");
}

//Method pause
// -----------------------------------------------------
// pause the player
V10Core.HTML5Video.prototype.pause = function(){
    switch(this.options.core.api){
        case "html5":
            this.player[0].pause();
            break;
        case "youtube":
            this.player.pauseVideo();
            break;
    }


    this.debugLog("HTML5Video :: " + this.options.core.name + " :: pause");
}

//Method stop
// -----------------------------------------------------
// stop the player
V10Core.HTML5Video.prototype.stop = function(){
    switch(this.options.core.api){
        case "html5":
            this.player[0].pause();
            this.player[0].currentTime = 0;
            break;
        case "youtube":
            this.player.stopVideo();
            break;
    }

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: stop");
}

//Method seek
// -----------------------------------------------------
// seek the player
V10Core.HTML5Video.prototype.seek = function(pos){
    switch(this.options.core.api){
        case "html5":
            this.player[0].currentTime = pos;
            break;
        case "youtube":
            this.player.seekTo(pos);
            break;
    }

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: seek", pos);
}

//Method change
// -----------------------------------------------------
// change the media in the player
V10Core.HTML5Video.prototype.change = function(data, params){

    switch(this.options.core.api){
        case "html5":
            for (key in params) {
                this.options.vParams[key] = params[key];
            }
            this.options.media.source = data;
            this.player[0].controls = this.options.vParams.controls;
            this.player[0].autoplay = this.options.vParams.autoplay;
            this.player[0].src = this.options.media.source;
            this.player[0].currentTime = 0;
            break;
        case "youtube":
            for (key in params) {
                this.options.youtube[key] = params[key];
            }
            this.options.youtube.videoId = data;
            this.player.loadVideoById(this.options.youtube.videoId);
            break;
    }

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: change", source, vParams);
}

//Function eventReady
// -----------------------------------------------------
// Init all the events of the player
V10Core.HTML5Video.prototype.eventReady = function(){
    var _this = this;

    switch(this.options.core.api){
        case "html5":
            this.player[0].addEventListener("playing", this.onStartCallback.bind(this));
            this.player[0].addEventListener("ended", this.onEndCallback.bind(this));
            this.player[0].addEventListener("seeked", this.onSeekedCallback.bind(this));
            this.player[0].addEventListener("play", this.onPlayCallback.bind(this));
            this.player[0].addEventListener("pause", this.onPauseCallback.bind(this));
            this.player[0].addEventListener("timeupdate", this.onCurrentTimeChangeCallback.bind(this));
            this.player[0].addEventListener("error", this.onErrorCallback.bind(this));
            this.player[0].addEventListener("loadedmetadata", this.onMetaLoadedCallback.bind(this));
            this.player[0].addEventListener("volumechange", this.onVolumeChangeCallback.bind(this));

            this.isMuted = this.player[0].muted;

            this.player.bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
                if(document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen){
                    _this.onEnterFullScreenCallback();
                }else{
                    _this.onExitFullScreenCallback();
                }
            });
            break;
        case "youtube":
            this.player.addEventListener("onReady", this.onPlayerReadyCallback.bind(this));
            this.player.addEventListener("onStateChange", this.onStateChangeCallback.bind(this));

            break;
    }

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: eventReady");
}

//Event onPlayerReadyCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onPlayerReadyCallback = function(){
    if(this.options.events.PLAYER_READY != null) this.options.events.PLAYER_READY(this);

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: PLAYER_READY");
}

//Event onStateChangeCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onStateChangeCallback = function(result){

    if(this.options.core.api == "youtube") {
        switch (result.data) {
            case -1:

                break;
            case 0:
                this.onEndCallback()
                break;
            case 1:
                this.onPlayCallback();
                break;
            case 2:
                this.onPauseCallback();
                break;
            case 3:

                break;
            case 5:

                break;
        }
    }
}

//Event onEndCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onEndCallback = function(){
    if(this.options.events.END != null) this.options.events.END(this);
    this.debugLog("HTML5Video :: " + this.options.core.name + " :: END");
}

//Event onStartCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onStartCallback = function(){
    if(this.options.events.START != null) this.options.events.START(this);
    this.debugLog("HTML5Video :: " + this.options.core.name + " :: START");
}

//Event onPlayCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onPlayCallback = function(){
    if(this.options.events.PLAY != null) this.options.events.PLAY(this);

    this.debugLog("HTML5Video :: " + this.options.core.name + " :: PLAY");
}

//Event onPauseCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onPauseCallback = function(){
    if(this.options.events.PAUSE != null) this.options.events.PAUSE(this);
    this.debugLog("HTML5Video :: " + this.options.core.name + " :: PAUSE");
}

//Event onSeekedCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onSeekedCallback = function(seek){
    if(this.options.events.SEEKED != null) this.options.events.SEEKED(this);
    this.debugLog("HTML5Video :: " + this.options.core.name + " :: SEEKED", seek);
}

//Event onCurrentTimeChangeCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onCurrentTimeChangeCallback = function(){
    if(this.options.events.CURRENT_TIME_CHANGE != null) this.options.events.CURRENT_TIME_CHANGE(this);
    this.debugLog("HTML5Video :: " + this.options.core.name + " :: CURRENT_TIME_CHANGE");
}

//Event onVolumeChangeCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onVolumeChangeCallback = function(volume){
    if(this.options.core.api == "html5") {
        if (this.isMuted != this.player[0].muted) {
            this.isMuted = this.player[0].muted;

            if (this.isMuted) {
                if (this.options.events.MUTE != null) this.options.events.MUTE(this);
                this.debugLog("HTML5Video :: " + this.options.core.name + " :: MUTE", volume);
            } else {
                if (this.options.events.UNMUTE != null) this.options.events.UNMUTE(this);
                this.debugLog("HTML5Video :: " + this.options.core.name + " :: UNMUTE", volume);
            }
        } else {
            if (this.options.events.VOLUME_CHANGE != null) this.options.events.VOLUME_CHANGE(this);
            this.debugLog("HTML5Video :: " + this.options.core.name + " :: VOLUME_CHANGE", volume);
        }
    }
}

//Event onMuteCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onMuteCallback = function(){
    if(this.options.events.MUTE != null) this.options.events.MUTE(this);
    this.debugLog("HTML5Video :: " + this.options.core.name + " :: MUTE");
}

//Event onUnMuteCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onUnMuteCallback = function(){
    if(this.options.events.UNMUTE != null) this.options.events.UNMUTE(this);
    this.debugLog("HTML5Video :: " + this.options.core.name + " :: UNMUTE");
}

//Event onEnterFullScreenCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onEnterFullScreenCallback = function(){
    if(this.options.events.ENTER_FULL_SCREEN != null) this.options.events.ENTER_FULL_SCREEN(this);
    this.debugLog("HTML5Video :: " + this.options.core.name + " :: ENTER_FULL_SCREEN");
}

//Event onExitFullScreenCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onExitFullScreenCallback = function(){
    if(this.options.events.EXIT_FULL_SCREEN != null) this.options.events.EXIT_FULL_SCREEN(this);
    this.debugLog("HTML5Video :: " + this.options.core.name + " :: EXIT_FULL_SCREEN");
}

//Event onErrorCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onErrorCallback = function(){
    if(this.options.events.ERROR != null) this.options.events.ERROR(this);
    this.debugLog("HTML5Video :: " + this.options.core.name + " :: ERROR");
}


//Event onMetaLoadedCallback
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onMetaLoadedCallback = function(metas){
    if(this.options.events.META_LOADED != null) this.options.events.META_LOADED(this);
    this.debugLog("HTML5Video :: " + this.options.core.name + " :: META_LOADED", metas);
}

//UI Event onUIEventPlay
// -----------------------------------------------------
V10Core.HTML5Video.prototype.onUIEventPlay = function(e){
    e.preventDefault();

    //Init the player by mod selected
    if(this.options.mod == V10Core.HTML5Video.mods.DEFAULT){
        if(this.ui.poster) this.ui.poster.hide();
        if(this.ui.btnPlay) this.ui.btnPlay.hide();
        this.initDefaultPlayer();
        this.eventReady();
    }
    if(this.options.mod == V10Core.HTML5Video.mods.FANCY_BOX){
        this.initFancyboxPlayer();
        this.eventReady();
    }
    if(this.options.mod == V10Core.HTML5Video.mods.ABSOLUTE){
        this.initAbsolutePlayer();
        this.eventReady();
    }
    if(this.options.mod == V10Core.HTML5Video.mods.FIXED){
        this.initFixedPlayer();
        this.eventReady();
    }
}

//Method global pause
// -----------------------------------------------------
// pause all players
V10Core.HTML5Video.pause = function(){
    for(var i = 0; i < V10Core.HTML5Video.count; i++){
        V10Core.HTML5Video.players[i][0].pause();
    }
}

//Method global stop
// -----------------------------------------------------
// stop all players
V10Core.HTML5Video.stop = function(){
    for(var i = 0; i < V10Core.HTML5Video.count; i++){
        V10Core.HTML5Video.players[i][0].stop();
    }
}

//Function debugLog
// -----------------------------------------------------
// Debug log utils
V10Core.HTML5Video.initPlayerByAttr = function(attr){
    var attrList = $("["+attr+"]:not([v10core-html5video-uAttr])").toArray();

    for(var i = 0; i < attrList.length; i++){
        V10Core.HTML5Video.attrCount++;
        var uContainerID = "v10core-html5video-uAttr";
        $(attrList[i]).attr(uContainerID,(V10Core.HTML5Video.count+1));
        var options = JSON.parse($(attrList[i]).attr(attr));
        if(!options.core) options.core = {};
        options.core.container = "["+uContainerID+"="+(V10Core.HTML5Video.count+1)+"]";

        var v = new V10Core.HTML5Video(options);
    }
}

//Function debugLog
// -----------------------------------------------------
// Debug log utils
V10Core.HTML5Video.prototype.debugLog = function(){
    if(this.options.core.debugMode){
        console.log(arguments);
    }
}

V10Core.HTML5Video.players = [];
V10Core.HTML5Video.YouTubeIframeAPIReady = false;
V10Core.HTML5Video.count = 0;
V10Core.HTML5Video.vPlayerAttribute = 'data-v10core-html5video';
V10Core.HTML5Video.mods = {DEFAULT:"default",ABSOLUTE:"absolute",FANCY_BOX:"fancybox",FIXED:"fixed"};
V10Core.HTML5Video.version = "1.2.0";

jQuery(document).ready(function(e){
    V10Core.HTML5Video.initPlayerByAttr(V10Core.HTML5Video.vPlayerAttribute);
});

function onYouTubeIframeAPIReady() {
    V10Core.HTML5Video.YouTubeIframeAPIReady = true;
}